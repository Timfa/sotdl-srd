<?php
    $paths = new stdClass();
    

    function GetFolderJSONContents($folder)
    {
        $result = [];
        $pathFiles = array_values(array_diff(scandir($folder), array('.', '..')));
        
        for ($i = 0; $i < count($pathFiles); $i++)
        {
            $path = file_get_contents($folder . $pathFiles[$i]);
            $jsonObj = json_decode($path);
            $jsonObj->sourceFile = $folder . $pathFiles[$i];
            array_push($result, $jsonObj);
        }

        return $result;
    }

    $paths->novice = GetFolderJSONContents("content/paths/novice/");
    $paths->expert = GetFolderJSONContents("content/paths/expert/");
    $paths->master = GetFolderJSONContents("content/paths/master/");
    $ancestries = GetFolderJSONContents("content/ancestries/");
    $monsters = GetFolderJSONContents("content/monsters/");
    
    $items = GetFolderJSONContents("content/items/");
    $religions = GetFolderJSONContents("content/religions/");
    $professions = GetFolderJSONContents("content/professions/");
    $traditionSpells = GetFolderJSONContents("content/spells/");
    usort($traditionSpells, 'levelComparator');
    $traditions = GetFolderJSONContents("content/traditions/");

    function levelComparator($o1, $o2)
    {
        return $o1->Level > $o2->Level;
    }


    function ShowPath($path)
    {
        echo '<div class="pathHeader" id="' . $path->Name . '">';
        echo '<div class="pathTitle">' . $path->Name . '<contributor class="contributor"> - Added by ' . $path->Contributor . '</contributor></div>';
        echo '<div class="fileSource"><a href="' . $path->sourceFile . '">.../' . $path->sourceFile . '</a> - ' . $path->Sourcebook . '</div>';
        echo '<div class="pathField" id="' . "Description " . $path->Name . '"><i>' . $path->Description . '</i></div>';
        echo '<div class="pathContent" id="' . "Content " . $path->Name . '">';
        
        echo '<div class="pathLevelCollection">';
        foreach($path->Levels as $key=>$value) 
        {
            echo '<div class="pathLevel" id="' . $key . " " . $path->Name . '"><div class="pathLevelTitle">' . $key . " " . $path->Name . '</div>';

            echo '<div class="pathField">';
            foreach($value as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';

            echo "</div>";
        }
        echo '</div>';

        if(isset($path->Training))
        {
            echo '<div class="pathBioCollection">';
            echo '<b>' . $path->Name . ' Training:</b> d6<br>';
            foreach($path->Training as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';
        }
        else if(isset($path->Development))
        {
            echo '<div class="pathBioCollection">';
            echo '<b>Story Development:</b> d6<br>';
            foreach($path->Development as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';
        }

        echo '</div>';
        
        echo '</div>';
    }

    function ShowMonster($path)
    {
        echo '<div class="pathHeader" id="' . $path->Name . '">';
        echo '<div class="pathTitle">' . $path->Name . '<contributor class="contributor"> - Added by ' . $path->Contributor . '</contributor></div>';
        echo '<div class="fileSource"><a href="' . $path->sourceFile . '">.../' . $path->sourceFile . '</a> - ' . $path->Sourcebook . '</div>';
        echo '<div class="pathField" id="' . "Description " . $path->Name . '"><i>' . $path->Description . '</i></div>';
        echo '<div class="pathContent" id="' . "Content " . $path->Name . '">';
        echo '<div class="pathField">';
            foreach($path->Attributes as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';
        foreach($path->Attacks as $key=>$value) 
        {
            echo '<div class="pathLevel" id="' . $key . " " . $path->Name . '"><div class="pathLevelTitle">' . $key . '</div>';

            echo '<div class="pathField">';
            foreach($value as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';

            echo "</div>";
            }
        echo '</div>';
        
        echo '</div>';
    }

    function ShowAncestry($path)
    {
        echo '<div class="pathHeader" id="' . $path->Name . '">';
        echo '<div class="pathTitle">' . $path->Name . '<contributor class="contributor"> - Added by ' . $path->Contributor . '</contributor></div>';
        echo '<div class="fileSource"><a href="' . $path->sourceFile . '">.../' . $path->sourceFile . '</a> - ' . $path->Sourcebook . '</div>';
        echo '<div class="pathField" id="' . "Description " . $path->Name . '"><i>' . $path->Description . '</i></div>';
        echo '<div class="pathContent" id="' . "Content " . $path->Name . '">';
        
        echo '<div class="pathLevelCollection">';
        foreach($path->Levels as $key=>$value) 
        {
            echo '<div class="pathLevel" id="' . $key . " " . $path->Name . '"><div class="pathLevelTitle">' . $key . " " . $path->Name . '</div>';

            echo '<div class="pathField">';
            foreach($value as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';

            echo "</div>";
        }
        echo '</div>';

        echo '<div class="pathBioCollection">';
        foreach($path->Bio as $key=>$value) 
        {
            echo '<div class="pathLevel" id="' . $key . " " . $path->Name . '"><div class="pathLevelTitle">' . $key . '</div>';

            echo '<div class="pathField">';
            foreach($value as $attr=>$text) 
            {
                echo '<b>' . $attr . ":</b> " . $text . "<br>";
            }
            echo '</div>';

            echo "</div>";
        }
        echo '</div>';
        echo '</div>';
        
        echo '</div>';
    }

    function ShowItem($item)
    {
        echo '<div class="pathHeader" id="' . $item->Name . '">';
        echo '<div class="pathTitle">' . $item->Name . '<contributor class="contributor"> - Added by ' . $item->Contributor . '</contributor></div>';;
        echo '<div class="fileSource"><a href="' . $item->sourceFile . '">.../' . $item->sourceFile . '</a> - ' . $item->Sourcebook . '</div>';
        
        echo '<div class="spellDescription" id="' . "Description " . $item->Name . '"><i>' . $item->Description . '</i></div>';
        
        echo '<div class="pathContent" id="' . "Content " . $item->Name . '">';
        
        echo '<div class="pathField">';
        foreach($item->Attributes as $attr=>$text) 
        {
            echo '<b>' . $attr . ":</b> " . $text . "<br>";
            
        }
        echo '</div>';

        echo '</div>';
        
        echo '</div>';
    }

    function ShowSpell($spell)
    {
        echo '<div class="pathHeader" id="' . $spell->Name . '">';
        echo '<div class="pathTitle">' . $spell->Name . '<contributor class="contributor"> - Added by ' . $spell->Contributor . '</contributor></div>Level ' . $spell->Level;
        echo '<div class="fileSource"><a href="' . $spell->sourceFile . '">.../' . $spell->sourceFile . '</a> - ' . $spell->Sourcebook . '</div>';
        echo '<div class="pathContent" id="' . "Content " . $spell->Name . '">';
        
        echo '<div class="pathField">';
        foreach($spell->Attributes as $attr=>$text) 
        {
            echo '<b>' . $attr . ":</b> " . $text . "<br>";
            
        }
        echo '</div>';

        echo '</div>';
        echo '<div class="spellDescription" id="' . "Description " . $spell->Name . '"><i>' . $spell->Description . '</i></div>';
        echo '</div>';
    }

    function ShowTradition($tradition, $allSpells)
    {
        echo '<div class="subSectionHeader" id="' . $tradition->Name . '">';
        echo '<div class="subSectionTitle">' . $tradition->Name . '<contributor class="contributor"> - Added by ' . $tradition->Contributor . '</contributor></div>' . $tradition->Attribute;
        echo '<div class="fileSource"><a href="' . $tradition->sourceFile . '">.../' . $tradition->sourceFile . '</a> - ' . $tradition->Sourcebook . '</div>';
        echo '<div class="pathField" id="' . "Description " . $tradition->Name . '"><i>' . $tradition->Description . '</i></div>';
        echo '<div class="pathContent" id="' . "Content " . $tradition->Name . '">';
        
        $levelCount = 0;
        echo '<div class="pathLevel">';
        echo '<div class="pathTitle">Level 0</div>';
        for($s = 0; $s < count($allSpells); $s++) 
        {
            if($allSpells[$s]->Tradition == $tradition->Name)
            {
                if($levelCount < $allSpells[$s]->Level)
                {
                    echo '</div><div class="pathLevel"><div class="pathTitle">Level ' . $allSpells[$s]->Level . '</div>';
                    $levelCount = $allSpells[$s]->Level;
                }
                
                ShowSpell($allSpells[$s]);
            }
        }
        echo '</div>';

        echo '</div>';
        echo '</div>';
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css"/>
    </head>

    <body>
        <div class="sectionHeader" id="ancestriesHeader">
            <div class="sectionTitle">Ancestries</div>
            <?php
                for ($i = 0; $i < count($ancestries); $i++)
                {
                    $ancestry = $ancestries[$i];
                    ShowAncestry($ancestry);
                }
            ?>
        </div>

        <div class="sectionHeader" id="pathsHeader">
            <div class="sectionTitle">Paths</div>
            <div class="subSectionHeader" id="novicePathHeader">
            <div class="subSectionTitle">Novice Paths</div>
            <?php
                for ($i = 0; $i < count($paths->novice); $i++)
                {
                    $path = $paths->novice[$i];
                    ShowPath($path);
                }
            ?>
            </div>

            <div class="subSectionHeader" id="expertPathHeader">
            <div class="subSectionTitle">Expert Paths</div>
            <?php
                for ($i = 0; $i < count($paths->expert); $i++)
                {
                    $path = $paths->expert[$i];
                    ShowPath($path);
                }
            ?>
            </div>

            <div class="subSectionHeader" id="masterPathHeader">
            <div class="subSectionTitle">Master Paths</div>
            <?php
                for ($i = 0; $i < count($paths->master); $i++)
                {
                    $path = $paths->master[$i];
                    ShowPath($path);
                }
            ?>
            </div>
        </div>

        <div class="sectionHeader" id="itemsHeader">
            <div class="sectionTitle">Items</div>
            <?php

                for ($i = 0; $i < count($items); $i++)
                {
                    $item = $items[$i];
                    ShowItem($item);
                }
            ?>
        </div>

        <div class="sectionHeader" id="magicHeader">
            <div class="sectionTitle">Magic</div>
            
            <?php

                for ($i = 0; $i < count($traditions); $i++)
                {
                    $path = $traditions[$i];
                    ShowTradition($path, $traditionSpells);
                }
            ?>
            </div>
        </div>

        <div class="sectionHeader" id="religionHeader">
            <div class="sectionTitle">Religions</div>
            
            <?php

                for ($i = 0; $i < count($religions); $i++)
                {
                    $path = $religions[$i];
                    ShowItem($path);
                }
            ?>
            </div>
        </div>

        <div class="sectionHeader" id="professionHeader">
            <div class="sectionTitle">Professions</div>
            
            <?php
                echo '<div class="pathField">';
                
                echo 'Profession Types:<br>';
                for ($i = 0; $i < count($professions); $i++)
                {
                    $path = $professions[$i];
                    echo '<b>' . ($i + 1) . "</b> - " . $path->Name . '<br>';
                }
                echo '</div>';

                for ($i = 0; $i < count($professions); $i++)
                {
                    $path = $professions[$i];
                    ShowItem($path);
                }
            ?>
            </div>
        </div>

        <div class="sectionHeader" id="monstersHeader">
            <div class="sectionTitle">Monsters</div>
            <?php
                for ($i = 0; $i < count($monsters); $i++)
                {
                    $monster = $monsters[$i];
                    ShowMonster($monster);
                }
            ?>
        </div>
    </body>
</html>